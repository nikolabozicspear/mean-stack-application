import { AuthService } from "./auth/auth.service";
import { OnInit } from "@angular/core";
import { Component } from "@angular/core";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  userIsAuthenticated = false;
  authListenerSubs: Subscription;

  constructor(private authService: AuthService, private router: Router) {}

  // bitno je stanje auth servica koji utice na auth stanje komponente koji moze biti autentikoval ili ne , prilikom svake promene stanja servisa (logout ili login) menjamo stanje servisa, obavestavamo
  // komponente preko listenera da usaglase svoje stanje, i stanje servisa upisujemo u locale storage.
  //Prilikom svakog pokretanja aplikacije poziva se local storage i proverava se da li imamo token i expire date i uodnosu na to setuje stanje servisa. Zatim komponenta pita servis za stanje
  // i usaglasava svoje stanje sa stanjem servisa.
  // Svako sledeca promena stanja u aplikaciji(login, logout ce promeniti local storage) i obavestiti svoje komponente da je stanje promenjeno


  ngOnInit() {
       this.authService.autoAuthUser(); //[gleda iz local storage i setuje stanje auth servise] ali problem je sto se prvo emituje pa se subscibujemo pa moramo i da pozovemo servis da vidimo stanje

    this.changePage(this.authService.getIsAuth());

    this.authListenerSubs = this.authService// prvo se emituje pa se onda subscribujemo u slucaju prvog pokretanja aplikacije, u drugim slucajevima kada promenimo stanje auth pokrenuce se callback i usaglasavamo stanje servisa i komponente
      .getAuthStatusListener()
      .subscribe(isAuthenticated => {      
        console.log('app subs')  
        this.changePage(isAuthenticated)// kada se logoutujemo ili loginujemo promenicemo stanje, upisati ili ispisati iz local storage i  poslati event koji ce se pokupiti ovde, samim time i pri sledecim
        // on initu  this.authService.autoAuthUser() ce pogledati iz lokal storage i tako podesiti stanje
      });
  }

  private changePage( isAuthenticated: boolean) {

    this.userIsAuthenticated = isAuthenticated;
    if (!this.userIsAuthenticated) {
      this.router.navigate(["login"]);
    } else if (this.userIsAuthenticated) {
      this.router.navigate(["post-list"]);
    }

  }

  ngOnDestroy() {
    
    this.authStatusSub.unsubscribe();
    
  }
}
