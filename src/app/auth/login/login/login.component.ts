import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  isLoading = false;

  private authStatusSub: Subscription;

  constructor(private authservice: AuthService, private router: Router) {

  

  }

  ngOnInit() {
    this.authStatusSub = this.authservice.getAuthStatusListener().subscribe( authstatus => {
      this.isLoading = authstatus;
    });
  }

  onLogin(form: NgForm) {
    if (form.invalid) {
      return
    }
    this.isLoading = true;
    this.authservice.login(form.value.email, form.value.password);
  }

  onRegister() {
    this.router.navigate(['signUp'])
  }

  ngOnDestroy(): void {
    this.authStatusSub.unsubscribe();
    
  }

}
