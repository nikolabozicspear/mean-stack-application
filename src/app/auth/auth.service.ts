import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthData } from "./auth-data.model";
import { tokenKey } from "@angular/core/src/view";
import { Subject } from "rxjs";
import { Router } from "@angular/router";

@Injectable({providedIn: "root"})
export class AuthService {

    private token: string;
    private isAuthenticated = false;
    private tokenTimer: NodeJS.Timer;
    private userId: string;

    private authStatuListener = new Subject<boolean>();

    getToken() {
        return this.token;
    }

    getIsAuth() {
        return this.isAuthenticated;
    }
    getUserId() {
        return this.userId;
    }

    constructor(private http: HttpClient, private router: Router) {
    }

    getAuthStatusListener() {
        return this.authStatuListener.asObservable();
    }

    createUser(email: string, password: string) {
        
        const authData: AuthData = {email: email, password: password};

        return this.http.post("http://localhost:3000/api/user/signup", authData)
            .subscribe(response => {        
                this.authStatuListener.next(false);
            }, error => {
                this.authStatuListener.next(false);

            });
    }

    login(email: string, password: string) {     

        const authData: AuthData = {email: email, password: password};

        this.http.post<{token: string, expiresIn: number, userId: string }>("http://localhost:3000/api/user/login", authData)
            .subscribe(response => {
              const token = response.token;
              this.token = token;
              if (token) {
                const expiresInDuridation = response.expiresIn;
                this.setAuthTimer(expiresInDuridation);
                this.authStatuListener.next(true);
                this.isAuthenticated = true;
                this.userId = response.userId;
                const now: Date = new Date();
                const expirationDate = new Date(now.getTime() + expiresInDuridation * 1000);
                this.saveAuthData(token, expirationDate, response.userId );               
                // this.router.navigate(['/']);                
              }
            }, error => {
                this.authStatuListener.next(false);
                this.router.navigate['login']
            });
    }

    autoAuthUser() {
        const authInformation = this.getAuthData();
        if(authInformation) {
            const now = new Date();
            const expiresIn = authInformation.expirationDate.getTime() - now.getTime();

            if (expiresIn > 0) {
                this.token = authInformation.token;
                this.userId = authInformation.userId;
                this.isAuthenticated = true;
                this.setAuthTimer(expiresIn / 1000)
                this.authStatuListener.next(true);
            }
        }
    }

    private setAuthTimer(duration: number) {
        this.tokenTimer = setTimeout(() => {
            this.logout();
        }, duration * 1000);
    }

    logout() {
        this.token = null;
        this.authStatuListener.next(false);
        this.isAuthenticated = false;
        this.userId = null;
        this.clearAuthData();
        clearTimeout(this.tokenTimer);
        // this.router.navigate(['/']);
    }

    private saveAuthData(token: string, expiratonDate: Date, userId: string) {
        localStorage.setItem('token', token);
        localStorage.setItem('expiration', expiratonDate.toISOString());
        localStorage.setItem('userId',userId);

    }

    private clearAuthData() {
        localStorage.removeItem('token');
        localStorage.removeItem('expiration');
        localStorage.removeItem('userId');
    }

    private getAuthData() {
        const token = localStorage.getItem('token')
        const expirationDate = localStorage.getItem('expiration');
        const userId = localStorage.getItem('userId');
        if (!token && !expirationDate) {
            return;
        }
        return {
            token: token,
            expirationDate: new Date(expirationDate),
            userId: userId
        }
    }
}
